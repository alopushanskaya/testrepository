# require 'spec_helper.rb'
#
# feature 'Newly registered Visitor creates project', js:true do
#   before(:all) do
#     @general_page = GeneralPage.new
#     @home_page = HomePage.new
#     @login_page = LoginPage.new
#     @myaccount_page = MyAccountPage.new
#     @registration_page = RegistrationPage.new
#     @project_creation_page = ProjectCreationPage.new
#     @search_results_page = SearchResultsPage.new
#   end

#   scenario 'Visitor registeres new defined user' do
#     register_user("Autotest11122234", "Qwerty123454")
#     logout_user
#   end
#
#   scenario 'The same defined user creates and verifies his defined project' do
#     login_user("Autotest11122234", "Qwerty123454")
#     create_project_named("ProjectNamed0101012234")
#     verify_newly_created_project
#     logout_user
#   end

#   scenario 'Visitor registeres random user' do
#     register_random_user
#     logout_user
#   end
#
#   scenario 'The same random user creates and verifies random project' do
#     login_random_user
#     create_random_project
#     verify_newly_created_project
#     logout_user
#   end
# end
