require 'spec_helper.rb'

feature 'Two users create and work on one project', js:true do
  before(:all) do
    @general_page = GeneralPage.new
    @home_page = HomePage.new
    @login_page = LoginPage.new
    @my_page = MyPage.new
    @myaccount_page = MyAccountPage.new
    @registration_page = RegistrationPage.new
    @project_creation_page = ProjectCreationPage.new
    @search_results_page = SearchResultsPage.new
    @selected_project_page = SelectedProjectPage.new
  end

  # scenario 'Two users are registered and work on one new project' do
  #   register_user("AUser1", "Qwerty1")
  #   create_project_named("AProject1")
  #   logout_user
  #   register_user("AUser2", "Qwerty1")
  #   logout_user
  #   login_user("AUser1", "Qwerty1")
  #   create_new_ticket_and_assign_user("ATicket1", "AUser2")
  #   logout_user
  #   login_user("AUser2", "Qwerty1")
  #   user_tracks_spent_hours_and_closes_the_ticket("4")
  #   logout_user
  #   login_user("AUser1", "Qwerty1")
  #   user_closes_the_project
  # end

  scenario 'Complicated autotest where Two users do E2E scenario' do
    two_users_E2E_scenario
  end


end
