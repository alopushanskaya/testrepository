module FeatureHelper

  def logout_user
    @general_page.logout.click
  end

  def register_user(us_login, us_pwd)
    visit '/'
    @home_page.reg_new_account.click
    @registration_page.user_login.set(us_login)
    @registration_page.user_password.set(us_pwd)
    @registration_page.user_password_confirmation.set(us_pwd)
    @registration_page.user_firstname.set(us_login)
    @registration_page.user_lastname.set(us_login)
    @registration_page.user_mail.set us_login +"@ciklum.com"
    @registration_page.user_language.select('English')
    @registration_page.submit.click
    expect(@myaccount_page).to have_account_registered
  end

  def login_user(us_login, us_pwd)
    visit '/login'
    @login_page.username.set(us_login)
    @login_page.password.set(us_pwd)
    @login_page.submit.click
  end

  def create_project_named(proj_name)
    @project = proj_name
    visit '/'
    @home_page.reg_new_project.click
    @project_creation_page.project_name.set @project
    @project_creation_page.continue.click
    # select(pr_name, :from => 'select[project_quick_jump_box]')
  end

  def verify_project_named(proj_name)
    visit '/'
    @general_page.search_field.set proj_name
    @general_page.search_field.send_keys :enter
    expect(@search_results_page).to have_search_results_counts
  end

  def register_random_user
    @@random_login = 'Testuser' + SecureRandom.random_number.to_s
    @@random_pwd = 'Qwerty1'
    @random_email = @@random_login +"@ciklum.com"
    visit '/'
    @home_page.reg_new_account.click
    @registration_page.user_login.set @@random_login
    @registration_page.user_password.set @@random_pwd
    @registration_page.user_password_confirmation.set @@random_pwd
    @registration_page.user_firstname.set @@random_login
    @registration_page.user_lastname.set "Surname"
    @registration_page.user_mail.set @random_email
    @registration_page.user_language.select('English')
    @registration_page.submit.click
    expect(@myaccount_page).to have_account_registered
  end

  def login_random_user
    visit '/login'
    @login_page.username.set @@random_login
    @login_page.password.set @@random_pwd
    @login_page.submit.click
  end

  def create_random_project
    @project = 'Project'+ DateTime.now.strftime('%d%m%H%M%S')
    # date = DateTime.now.strftime('%d%m%H%M%S') // Time.now.to_i.to_s
    visit '/'
    @home_page.reg_new_project.click
    @project_creation_page.project_name.set @project
    @project_creation_page.continue.click
  end

  def verify_newly_created_project
    visit '/'
    @general_page.search_field.set @project
    @general_page.search_field.send_keys :enter
    expect(@search_results_page).to have_search_results_counts
    expect(@search_results_page).to have_content "Results (1)"
  end

  def create_new_ticket_and_assign_user(ticket_name, us_login)
    visit '/'
    @general_page.jump_to_project.select(@project)
    @selected_project_page.move_to_settings.click
    @selected_project_page.move_to_members.click
    @selected_project_page.new_member.click
    @selected_project_page.input_assigned_user.set us_login
    sleep 2
    @selected_project_page.select_assigned_user.set(true)
    @selected_project_page.select_new_user_role.set(true)
    sleep 2
    @selected_project_page.add_button.click
    sleep 2
    @selected_project_page.move_to_new_issue_creation.click
    @selected_project_page.tracker_type.select('Feature')
    @selected_project_page.subject_field.set ticket_name
    @selected_project_page.issue_status.select('New')
    @selected_project_page.issue_priority.select('High')
    @selected_project_page.assignee.select('us_login' + ' ' + 'us_login')
    @selected_project_page.submit_and_continue.click
    expect(@selected_project_page).to have_issue_created
  end

  def user_tracks_spent_hours_and_closes_the_ticket(hours_to_track)
    @@time_to_track = hours_to_track + '.0'
    visit '/'
    @home_page.my_page.click
    @my_page.issues_assigned_to_me.click
    @my_page.select_ticket.click
    @my_page.edit_button.click
    @my_page.spent_hours.set @@time_to_track
    @my_page.ticket_status.select('Closed')
    @my_page.activity_type.select('Development')
    @my_page.percentage_done.select('100 %')
    @my_page.submit.click
    expect(@my_page).to have_content 'Successful update.'
  end


  def user_closes_the_project
    visit '/'
    @general_page.jump_to_project.select(@project)
    @selected_project_page.close_ikon.click
    sleep 1
    @selected_project_page.accept_confirm
    expect(@selected_project_page).to have_content 'This project is closed and read-only.'
  end

  def two_users_E2E_scenario
    #register user 1
    @@random_login1 = 'Testuser' + SecureRandom.random_number.to_s
    @@random_login2 = 'Testuser' + SecureRandom.random_number.to_s
    @@ticket_name = 'Ticket'+ DateTime.now.strftime('%d%m%H%M%S')
    @@random_pwd = 'Qwerty1'
    @@random_email1 = @@random_login1 +"@ciklum.com"
    @@random_email2 = @@random_login2 +"@ciklum.com"
    visit '/'
    @home_page.reg_new_account.click
    @registration_page.user_login.set @@random_login1
    @registration_page.user_password.set @@random_pwd
    @registration_page.user_password_confirmation.set @@random_pwd
    @registration_page.user_firstname.set @@random_login1
    @registration_page.user_lastname.set 'Surname'
    @registration_page.user_mail.set @@random_email1
    @registration_page.user_language.select('English')
    @registration_page.submit.click
    sleep 1
    expect(@myaccount_page).to have_account_registered

    #create random project
    @project = 'Project' + DateTime.now.strftime('%d%m%H%M%S')
    visit '/'
    @home_page.reg_new_project.click
    @project_creation_page.project_name.set @project
    @project_creation_page.continue.click

    @general_page.logout.click

    #register user 2
    visit '/'
    @home_page.reg_new_account.click
    @registration_page.user_login.set @@random_login2
    @registration_page.user_password.set @@random_pwd
    @registration_page.user_password_confirmation.set @@random_pwd
    @registration_page.user_firstname.set @@random_login2
    @registration_page.user_lastname.set 'Surname'
    @registration_page.user_mail.set @@random_email2
    @registration_page.user_language.select('English')
    @registration_page.submit.click
    expect(@myaccount_page).to have_account_registered

    @general_page.logout.click

    #login user 1
    visit '/login'
    @login_page.username.set @@random_login1
    @login_page.password.set @@random_pwd
    @login_page.submit.click


    #create_new_ticket_and_assign_user("ATicket1", "AUser2")
    visit '/'
    @general_page.jump_to_project.select(@project)
    @selected_project_page.move_to_settings.click
    @selected_project_page.move_to_members.click
    @selected_project_page.new_member.click
    @selected_project_page.input_assigned_user.set @@random_login2
    sleep 2
    @selected_project_page.select_assigned_user.set(true)
    sleep 2
    @selected_project_page.select_new_user_role.set(true)
    sleep 2
    @selected_project_page.add_button.click
    sleep 2
    @selected_project_page.move_to_new_issue_creation.click
    @selected_project_page.tracker_type.select('Feature')
    @selected_project_page.subject_field.set @@ticket_name
    @selected_project_page.issue_status.select('New')
    @selected_project_page.issue_priority.select('High')
    @@users_initials = @@random_login2 + ' Surname'
    @selected_project_page.assignee.select(@@users_initials)
    @selected_project_page.submit_and_continue.click
    expect(@selected_project_page).to have_issue_created

    @general_page.logout.click

    #login user 2
    visit '/login'
    @login_page.username.set @@random_login2
    @login_page.password.set @@random_pwd
    @login_page.submit.click

    #user_tracks_spent_hours_and_closes_the_ticket("4")
    visit '/'
    @home_page.my_page.click
    @my_page.issues_assigned_to_me.click
    @my_page.select_ticket.click
    @my_page.edit_button.click
    @my_page.spent_hours.set "5.0"
    @my_page.ticket_status.select('Closed')
    @my_page.activity_type.select('Development')
    @my_page.percentage_done.select('100 %')
    @my_page.submit.click
    expect(@my_page).to have_content 'Successful update.'

    @general_page.logout.click

    #login user 1
    visit '/login'
    @login_page.username.set @@random_login1
    @login_page.password.set @@random_pwd
    @login_page.submit.click

    #user_closes_the_project
    visit '/'
    @general_page.jump_to_project.select(@project)
    @selected_project_page.close_ikon.click
    sleep 1
    #@selected_project_page.evaluate_script('window.confirm = function() { return true; }')
    @selected_project_page.accept_confirm

    expect(@selected_project_page).to have_content 'This project is closed and read-only.'

    @general_page.logout.click

  end



end
