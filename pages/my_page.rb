class MyPage < SitePrism::Page
  element :issues_assigned_to_me, "a[href='/issues?assigned_to_id=me&set_filter=1&sort=priority%3Adesc%2Cupdated_on%3Adesc']"
  element :select_ticket, :xpath, "//td[@class='id']/a"
  element :edit_button, :xpath, "//a[@class = 'icon icon-edit' and @accesskey = 'e' ]"
  element :spent_hours, "input[id = 'time_entry_hours']"
  element :ticket_status, "select[id='issue_status_id']"
  element :activity_type, "select[id='time_entry_activity_id']"
  element :percentage_done, "select[id='issue_done_ratio']"
  element :submit, "input[type = 'submit']"
end
