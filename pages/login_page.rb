class LoginPage < SitePrism::Page
  element :username, "input[id='username']"
  element :password, "input[name='password']"
  element :submit, "input[type='submit']"
end
