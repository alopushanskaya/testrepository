class GeneralPage < SitePrism::Page
  element :logout, "a[class='logout']"
  element :search_field, "input[name='q']"
  element :jump_to_project, "select[id='project_quick_jump_box']"
end
