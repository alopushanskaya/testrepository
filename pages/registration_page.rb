class RegistrationPage < SitePrism::Page
  element :user_login, "input[id='user_login']"
  element :user_password, "input[id='user_password']"
  element :user_password_confirmation, "input[id='user_password_confirmation']"
  element :user_firstname, "input[id='user_firstname']"
  element :user_lastname, "input[id='user_lastname']"
  element :user_mail, "input[id='user_mail']"
  element :user_language, "select[id='user_language']"
  element :submit, "input[type='submit']"
end
