class HomePage < SitePrism::Page
  element :reg_new_account, :xpath, "//a[contains(text(),'register for an account')]"
  element :reg_new_project,  "a[href='/projects/new']"
  element :my_page,  "a[class='my-page']"
end
