class ProjectCreationPage < SitePrism::Page
  element :project_name, "input[name='project[name]']"
  element :continue, "input[name='continue']"
end
