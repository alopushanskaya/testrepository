class SearchResultsPage < SitePrism::Page
  element :search_results_counts, "div[id='search-results-counts']"
end
